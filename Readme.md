# Compilation and installation on JUQUEEN

For starting the compilation and installation process on JUQUEEN, you only have to invoke

    :::bash
    git clone https://bitbucket.org/dasmy/paraviewsuperbuild_bgq.git ./pvsuperbuild
    cd pvsuperbuild/pv_juqueen_installscript/
    ./paraview.sh
    
